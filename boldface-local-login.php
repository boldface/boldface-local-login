<?php
/**
 * Plugin Name: Boldface Local Login
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-login/
 * Description: Automatic login on sites hosted locally.
 * Version: 1.0.0
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI: https://www.gnu.org/licenses/gpl-2.0.en.html
 * Domain Path: /languages
 * Text Domain: boldface-local-login
*/

if( 'https://localhost' !== site_url( '', 'https' ) ) {
  add_action( 'admin_init', function() {
    deactivate_plugins( plugin_basename( __FILE__ ) );
    wp_die( __( 'This plugin must be installed locally.', 'boldface-local-login' ) );
  } );
} else {
  if( ! function_exists( 'wp_validate_auth_cookie' ) ) {
    function wp_validate_auth_cookie() {
      $args = [
        'role__in' => [ 'administrator' ],
      ];
      $admins = get_users( $args );
      if( count( $admins ) > 0 ) {
        foreach( $admins as $admin ) {
          return $admin->ID;
        }
      }
      wp_die( __( 'There appears to be no administrator users.', 'boldface-local-login' ) );
    }
  }
}
